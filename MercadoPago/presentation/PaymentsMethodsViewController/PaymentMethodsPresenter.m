//
//  PaymentMethodsPresenter.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "PaymentMethodsPresenter.h"

@implementation PaymentMethodsPresenter
- (void)getPaymentsMethods:(PresenterOperationSuccess)success error:(PresenterOperationError)error{
  [self.interactor getPaymentMethods:success error:error];
  
}
- (void)getBanksForPayment:(PaymentMethod *)payment success:(PresenterOperationSuccess)success error:(PresenterOperationError)error{
  [self.interactor getBanksForPayment:payment success:success error:error];
}
- (void)getFeesWithAmount:(int)amount payment:(PaymentMethod *)payment bank:(Bank *)bank success:(PresenterOperationSuccess)success error:(PresenterOperationError)error{
  [self.interactor getFeesWithAmount:amount payment:payment bank:bank success:success error:error];
}
- (void)getPaymentDetail:(PresenterOperationSuccess)success error:(PresenterOperationError)error{
  [self.interactor fetchFirstObjectOfClass:Payment.class sucess:success error:error];
}
- (void)savePaymentMethod:(PaymentMethod *)pm success:(PresenterOperationSuccess)success error:(PresenterOperationError)error{
  [self.interactor savePaymentMethod:pm success:success error:error];
}
- (void)saveBank:(Bank *)bk success:(PresenterOperationSuccess)success error:(PresenterOperationError)error{
  [self.interactor saveBank:bk success:success error:error];
}
@end
