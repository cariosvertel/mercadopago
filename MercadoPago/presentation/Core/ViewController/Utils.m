//
//  Utils.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/16/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "Utils.h"

@implementation Utils
+ (NSString *)formatAmount:(NSString *)amount
{
  
  // format Refund Amount values
  NSString *noCurrency = [self applyRegexWithText:amount pattern:@"[$,.]" template:@""];
  
  if ([noCurrency isEqualToString:@""]) noCurrency = @"0";
  
  double number = [noCurrency doubleValue];
  NSString *formatted = [NSString stringWithFormat:@"$%@", [Utils formatCurrencyAmount:number]];
  return formatted;
}
+ (NSString *) applyRegexWithText:(NSString *)text
                          pattern:(NSString *)pattern
                         template:(NSString *)templateReplace
{
  NSError *error;
  NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
  NSString *modifiedString = [regex stringByReplacingMatchesInString:text options:0 range:NSMakeRange(0, [text length]) withTemplate:templateReplace];
  return modifiedString;
}
+ (NSString *)formatCurrencyAmount:(double)amount
{
  NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
  [numberFormatter setFormatterBehavior: NSNumberFormatterBehavior10_4];
  numberFormatter.maximumFractionDigits = 0;
  [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
  return [numberFormatter stringFromNumber:@(amount)];
}
+(NSString *)extractOnlyDigits:(NSString *)text{
  return [self applyRegexWithText:text pattern:@"\\D" template:@""];
}

@end
