//
//  AmountInteractor.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "AmountInteractor.h"
#import "Payment.h"
NSString *const kAmountInteractorDomain =@"AmountInteractorDomain";
@implementation AmountInteractor
- (void)saveAmount:(NSNumber *)number success:(InteractorOperationSuccess)success error:(InteractorOperationError)error{
  // validations
  if ([number intValue] == 0){
    error([NSError errorWithDescription:@"La cantidad debe ser mayor a 0" domain:kAmountInteractorDomain]);
  }
  
  // do some work<#code#>
  [self fetchFirstObjectOfClass:Payment.class sucess:^(id result) {
    dispatch_async(self.scheduler, ^{
      Payment *paymentObject = result;
      if(paymentObject == nil){
        paymentObject = [Payment new];
      }
      paymentObject.amount = number;
      [self.storage.local save:paymentObject];
      dispatch_async(dispatch_get_main_queue(), ^{
        success([[InteractorOperation alloc] initWithResult:OperationRusultOK]);
      });
    });
  } error:nil];
}

@end
