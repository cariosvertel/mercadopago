//
//  PaymentMethodsPresenter.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "BasePresenter.h"
#import "PaymentMethodsInteractor.h"

@interface PaymentMethodsPresenter : BasePresenter<PaymentMethodsInteractor *>
- (void)getPaymentsMethods:(PresenterOperationSuccess)success error:(PresenterOperationError)error;
- (void)getBanksForPayment:(PaymentMethod *)payment success:(PresenterOperationSuccess)success error:(PresenterOperationError)error;
- (void) getFeesWithAmount:(int)amount payment:(PaymentMethod *)payment bank:(Bank *)bank success:(PresenterOperationSuccess)success error:(PresenterOperationError)error;
-(void)getPaymentDetail:(PresenterOperationSuccess)success error:(PresenterOperationError)error;
-(void)savePaymentMethod:(PaymentMethod *)pm success:(PresenterOperationSuccess)success error:(PresenterOperationError)error;
- (void)saveBank:(Bank *)bk success:(PresenterOperationSuccess)success error:(PresenterOperationError)error;
@end
