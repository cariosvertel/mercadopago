//
//  Fee.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/16/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "BaseObject.h"

@interface Fee : BaseObject
@property(nonatomic, strong) NSString *identifier;
@property(nonatomic, strong) NSString *message;
@end
