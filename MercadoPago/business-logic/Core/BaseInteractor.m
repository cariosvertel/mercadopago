//
//  BaseInteractor.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "BaseInteractor.h"

@implementation BaseInteractor
@synthesize scheduler = _scheduler;
@synthesize storage = _storage;

- (instancetype) initWithScheduler:(dispatch_queue_t)scheduler storage:(UserDataStorage *)storage{
  self = [super init];
  if (self){
    _scheduler = scheduler;
    _storage = storage;
    
  }
  return self;
}
+(id)instance{
  UserDataStorage *storage = [UserDataStorage instance];
  return [[self alloc] initWithScheduler:[self concurrentQueue] storage:storage];
}
+(dispatch_queue_t)concurrentQueue{
  static dispatch_queue_t current;
  static dispatch_once_t onceToken;
  
  dispatch_once(&onceToken, ^{
    current = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
  });
  return current;
}
- (id)current{
  return self;
}
-(void)fetchFirstObjectOfClass:(Class)clazz sucess:(InteractorOperationSuccess)success error:(InteractorOperationError)error{
  dispatch_async(self.scheduler, ^{
    id object = [self.storage.local fetchFirstObjectOfClass:clazz];
    dispatch_async(dispatch_get_main_queue(), ^{
      success(object);
    });
  });
}
- (void)runOnMainThread:(MainThreadBlock)block{
  dispatch_async(dispatch_get_main_queue(), block);
}

@end
