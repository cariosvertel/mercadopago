//
//  RLMPayment.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Realm/Realm.h>
#import "RealmObjectConverter.h"
#import "Payment.h"
#import "RLMFee.h"
#import "RLMPaymentMethod.h"
#import "RLMBank.h"

@interface RLMPayment : RLMObject<RealmObjectConverter>
@property NSString *identifier;
@property int amount;
@property RLMBank *bank;
@property RLMPaymentMethod *paymentMethod;

@end
