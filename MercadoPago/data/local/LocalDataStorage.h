//
//  LocalDataStorage.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocalStorage.h"
#import <Realm/Realm.h>
#import "Payment.h"
#import "RLMPayment.h"


@interface LocalDataStorage : NSObject<LocalStorage>




@end
