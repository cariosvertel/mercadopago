//
//  BaseTableViewCell.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/16/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol PaymentMethodTableViewCellDelegate<NSObject>
-(void)didTapOnCell:(id)object;
@end

@interface BaseTableViewCell : UITableViewCell
@property (weak, nonatomic) id object;
@property (weak, nonatomic) id<PaymentMethodTableViewCellDelegate> delegate;

@end
