//
//  PaymentMethod.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "RLMPaymentMethod.h"

@implementation RLMPaymentMethod
+(NSArray<RLMPaymentMethod *> *)decode:(NSArray<NSDictionary *> *)dictionary{
  
  NSMutableArray *newArray = [NSMutableArray new];
  for (NSDictionary *paymentAsJson in dictionary){
    [newArray addObject:[RLMPaymentMethod decodeSingle:paymentAsJson]];
  }
  return newArray;
}
+(RLMPaymentMethod *)decodeSingle:(NSDictionary *)payment{
  RLMPaymentMethod *newPayment = [[RLMPaymentMethod alloc] init];
  newPayment.name = payment[@"name"];
  newPayment.identifier = payment[@"id"];
  newPayment.methodType = payment[@"payment_type_id"];
  newPayment.imageUrl = payment[@"thumbnail"];
  return newPayment;
}
+(NSString *)primaryKey {
  return @"identifier";
}

#pragma mark - Realm object converter
-(id)fromRealmObject{
  PaymentMethod *newPaymentMethod = [[PaymentMethod alloc] initWithIdentifier:self.identifier];
  newPaymentMethod.imageUrl = self.imageUrl;
  newPaymentMethod.name = self.name;
  newPaymentMethod.methodType = self.methodType;
  return newPaymentMethod;
}
+(id)toRealmObject:(PaymentMethod *)obj{
  RLMPaymentMethod *pm = [[RLMPaymentMethod alloc] init];
  pm.identifier = obj.identifier;
  pm.name = obj.name;
  pm.imageUrl = obj.imageUrl;
  pm.methodType = obj.methodType;
  return pm;
}

@end
