//
//  NetworkAdapter.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#ifndef NetworkAdapter_h
#define NetworkAdapter_h

typedef void(^NetworkCallback)(NSDictionary *result, NSError *error);

@protocol NetworkAdapter<NSObject>

- (void)url:(NSString *)url parameters:(NSDictionary *)parameters callback:(NetworkCallback)callback;
- (void)get:(NSString *)url parameters:(id)parameters callback:(NetworkCallback)callback;
@end
#endif /* NetworkAdapter_h */
