//
//  FeeTableViewCell.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/15/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "FeeTableViewCell.h"
NSString *const kFeeTableViewCellIdentifier = @"feeTableViewCell";
@implementation FeeTableViewCell
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setObject:(Fee *)f{
  [super setObject:f];
  self.feeAmount.text = f.message;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
