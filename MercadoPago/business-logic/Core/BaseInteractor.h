//
//  BaseInteractor.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDataStorage.h"
#import "UserDataStorage.h"
#import "InteractorOperation.h"

typedef void(^InteractorOperationSuccess)(id result);
typedef void(^InteractorOperationError)(NSError *error);
typedef void(^MainThreadBlock)(void);


@interface BaseInteractor : NSObject
@property (nonatomic, strong, readonly) dispatch_queue_t scheduler;
@property (nonatomic, strong, readonly) UserDataStorage *storage;
- (instancetype) initWithScheduler:(dispatch_queue_t )schedulaer storage:(UserDataStorage *)storage;
-(void)fetchFirstObjectOfClass:(Class)clazz sucess:(InteractorOperationSuccess)success error:(InteractorOperationError)error;
+ (instancetype)instance;
+(dispatch_queue_t)concurrentQueue;
- (void)runOnMainThread:(MainThreadBlock)block;
@end
