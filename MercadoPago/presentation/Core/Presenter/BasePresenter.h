//
//  BasePresenter.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseInteractor.h"

typedef void(^PresenterOperationSuccess)(id result);
typedef void(^PresenterOperationError)(NSError *error);


@interface BasePresenter<__covariant InteractorType> : NSObject
@property (nonatomic, strong, readonly) InteractorType interactor;
- (instancetype) initWithInteractor:(InteractorType)interactor;


@end
