//
//  UserDataStorage.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "UserDataStorage.h"

@implementation UserDataStorage
@synthesize local = _local;
@synthesize cloud = _cloud;

-(instancetype)initWithLocal:(id<LocalStorage>)local cloud:(id<CloudStorage>)cloud{
  self = [super init];
  if (self){
    _local = local;
    _cloud = cloud;
  }
  return self;
}
+(UserDataStorage *)instance{
  static UserDataStorage * current;
  static dispatch_once_t onceToken;
  
  dispatch_once(&onceToken, ^{
    LocalDataStorage *local = [[LocalDataStorage alloc] init];
    AlamofireAdapter *adapter = [[AlamofireAdapter alloc] init];
    CloudDataStorage *cloud = [[CloudDataStorage alloc] initWithAdapter:adapter];
    current = [[self alloc] initWithLocal:local cloud:cloud];
  });
  return current;
  
}

@end
