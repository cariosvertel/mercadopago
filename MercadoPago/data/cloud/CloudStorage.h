//
//  CloudStorage.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#ifndef CloudStorage_h
#define CloudStorage_h
typedef void(^CloudOperationSuccess)(id result);
typedef void(^CloudOperationError)(NSError *error);
@protocol CloudStorage<NSObject>
- (void) getPaymentsMethods:(CloudOperationSuccess)success failed:(CloudOperationError)failed;
- (void) getBankForPaymentId:(NSString *)paymentId success:(CloudOperationSuccess)success failed:(CloudOperationError)failed;
- (void) getFeesWithAmount:(NSString *)amount paymentId:(NSString *)paymentId bank:(NSString *)bankId success:(CloudOperationSuccess)success failed:(CloudOperationError)failed;
@end

#endif /* CloudStorage_h */
