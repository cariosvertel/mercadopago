//
//  PaymentMethodsViewController.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "BaseViewController.h"
#import "PaymentMethodsPresenter.h"
#import "PaymentMethodsInteractor.h"
#import "Constants.h"

@interface PaymentMethodsViewController : BaseViewController<PaymentMethodsPresenter *>

@end
