//
//  UserDataStorage.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "LocalStorage.h"
//#import "CloudStorage.h"
#import "LocalDataStorage.h"
#import "CloudDataStorage.h"
#import "AlamofireAdapter.h"

@interface UserDataStorage : NSObject
// properties
@property(nonatomic, readonly, strong) id<LocalStorage> local;
@property(nonatomic, readonly, strong) id<CloudStorage> cloud;

// methods
-(instancetype)initWithLocal:(id<LocalStorage>)local cloud:(id<CloudStorage>)cloud;
+(UserDataStorage *)instance;
@end
