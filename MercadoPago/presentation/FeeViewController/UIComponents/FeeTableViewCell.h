//
//  FeeTableViewCell.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/15/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Fee.h"
#import "BaseTableViewCell.h"
FOUNDATION_EXPORT NSString *const kFeeTableViewCellIdentifier;
@interface FeeTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *feeAmount;

@end
