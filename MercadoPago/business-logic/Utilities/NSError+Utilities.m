//
//  NSError+Utilities.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/15/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "NSError+Utilities.h"

@implementation NSError (Utilities)
+(NSError *) errorWithDescription:(NSString *)description domain:(NSString *)domain{
  NSDictionary *info = @{NSLocalizedDescriptionKey:description};
  NSError *error = [NSError errorWithDomain:domain code:0 userInfo:info];
  return error;
}

@end
