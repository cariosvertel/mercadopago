//
//  PaymentMethodsViewController.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "PaymentMethodsViewController.h"
#import "RLMPaymentMethod.h"
#import "PaymentMethodTableViewCell.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <SVProgressHUD/SVProgressHUD.h>

NSString *const kGoToBankController = @"goToBank";
@interface PaymentMethodsViewController ()<UITableViewDelegate,UITableViewDataSource, PaymentMethodTableViewCellDelegate>
@property(nonatomic, strong) NSArray<PaymentMethod *> *payments;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation PaymentMethodsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  self.tableView.delegate = self;
  self.tableView.dataSource = self;
  self.title = kPaymentMethodControllerTitle;
  
  __weak PaymentMethodsViewController *weakSelf = self;
  
  [SVProgressHUD showWithStatus:kDownloadingPaymentMethodsMessage];
  [self.presenter getPaymentsMethods:^(NSArray<PaymentMethod *> *payments){
    weakSelf.payments = payments;
    [weakSelf.tableView reloadData];
    [SVProgressHUD dismiss];
    
  } error:^(NSError *error){
    [SVProgressHUD dismiss];
    [weakSelf showErrorWith:error];
  }];

}
- (void)viewWillAppear:(BOOL)animated{
  [super viewWillAppear:animated];
}
- (id)createPresenter{
  PaymentMethodsInteractor *interactor = [PaymentMethodsInteractor instance];
  return [[PaymentMethodsPresenter alloc] initWithInteractor:interactor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewCell Delegate & Datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
  return 48.0f;
  
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
  return 1.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  
  return self.payments.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  // for some reason this method doesn't work, I couldn't work with it.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  
  PaymentMethod *method = self.payments[indexPath.row];
  PaymentMethodTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kPaymentMethodCellIdentifier];
  [cell setObject:method];
  cell.delegate = self;
  return cell;
}
#pragma mark - Payment method
-(void)didTapOnCell:(PaymentMethod *)payment{
  __weak PaymentMethodsViewController *weakSelf = self;
  [self.presenter savePaymentMethod:payment success:^(id result){
    [weakSelf performSegueWithIdentifier:kGoToBankController sender:nil];
  } error:^(NSError *error){
    [weakSelf showErrorWith:error];
  }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
