//
//  AmountPresenter.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "BasePresenter.h"
#import "AmountInteractor.h"
#import "Utils.h"

@interface AmountPresenter : BasePresenter<AmountInteractor *>
- (void) saveAmount:(NSString *)amount success:(PresenterOperationSuccess)success error:(PresenterOperationError)error;
- (NSString *)formatAmount:(NSString *)text;
@end
