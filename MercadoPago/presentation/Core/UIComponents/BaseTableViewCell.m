//
//  BaseTableViewCell.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/16/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "BaseTableViewCell.h"

@implementation BaseTableViewCell
@synthesize object = _object;
- (void)awakeFromNib {
  [super awakeFromNib];
  // Initialization code
  UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnCell:)];
  [self.contentView addGestureRecognizer:recognizer];
}
- (void)didTapOnCell:(UITapGestureRecognizer *)aRecognizer{
  [self.delegate didTapOnCell:self.object];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setObject:(id)object{
  _object = object;
}

@end
