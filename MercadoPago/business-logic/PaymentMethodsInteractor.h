//
//  PaymentMethodsInteractor.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "BaseInteractor.h"

@interface PaymentMethodsInteractor : BaseInteractor
- (void) getPaymentMethods:(InteractorOperationSuccess)success error:(InteractorOperationError)error;
- (void) getBanksForPayment:(PaymentMethod *)payment success:(InteractorOperationSuccess)success error:(InteractorOperationError)error;
- (void) getFeesWithAmount:(int)amount payment:(PaymentMethod *)payment bank:(Bank *)bank success:(InteractorOperationSuccess)success error:(InteractorOperationError)error;
- (void)savePaymentMethod:(PaymentMethod *)pm success:(InteractorOperationSuccess)success error:(InteractorOperationError)error;
- (void)saveBank:(Bank *)bank success:(InteractorOperationSuccess)success error:(InteractorOperationError)error;
@end
