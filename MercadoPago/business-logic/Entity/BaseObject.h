//
//  BaseObject.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/15/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseObject : NSObject
@property(nonatomic, strong,readonly) NSString *identifier;
-(instancetype)initWithIdentifier:(NSString *)identifier;
@end
