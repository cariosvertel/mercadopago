Mercado Pago demo iOS app
===

###Arquitectura propuesta
Este proyecto se diseñó bajo la arquitectura Clean. En consecuencia de eso, hemos establecido la estructura del proyecto en modo de capas organizadas desde la más externa hasta la mas internas. Cabe resaltar aquí que algunas capas (presentación) se pudo haber precindido de ellas debido a lo poco extenso que es la demo app, pero quise hacerlo de esta manera para dejar en claro facil escalabilidad que tiene esta arquitectura. 

* **Presentation:**  
Se encarga de ajustar los datos de la capa de la logica del negocio con el fin de que sea amigable con la vista. Aquí no se hizo un trabajo representativo ya que la información que viene desde el servidor es bastante amigable para el entendimiento del usuario.

* **Business-logic**  
Se traza aqui toda la logica del negocio de la app. Validaciones, filtrado de datos, etc.

* **Data**  
Basado en el patrón `UserRepository` esta capa se encarga netamente de la extracción de los objectos desde el cloud o guardarlos localmente.

### Instalación 
Decidí usar `Carthage` como administrador de paquetes del proyecto. Entonces antes de correr el proyecto en `Xcode` ejecuta esta comando en el terminal:

```
$ carthage update
```

No deberías tener más problemas. Sin embargo sino tienes Carthage instalado, puedes conseguirlo [aquí](https://github.com/Carthage/Carthage#installing-carthage).




 

