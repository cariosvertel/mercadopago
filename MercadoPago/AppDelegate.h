//
//  AppDelegate.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/13/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

