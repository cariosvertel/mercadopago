//
//  AmountPresenterTest.m
//  MercadoPagoTests
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FakeLocalStorage.h"
#import "FakeNetworkAdapter.h"
#import "CloudDataStorage.h"
#import "UserDataStorage.h"
#import "FakeAmountView.h"
#import "AmountPresenter.h"
#import "AmountInteractor.h"

@interface AmountPresenterTest : XCTestCase
@property(nonatomic, strong) AmountPresenter *presenter;
@property(nonatomic, strong) FakeLocalStorage *localStorage;
@end

@implementation AmountPresenterTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.

  // storage system
  self.localStorage = [[FakeLocalStorage alloc] init];
  FakeNetworkAdapter *adapter = [[FakeNetworkAdapter alloc] init];
  CloudDataStorage *cloud = [[CloudDataStorage alloc] initWithAdapter:adapter];
  UserDataStorage *storage = [[UserDataStorage alloc] initWithLocal:self.localStorage cloud:cloud];
  NSObject *object = [[NSObject alloc] init];
  AmountInteractor *interactor = [[AmountInteractor alloc] initWithScheduler:object storage:storage];
  self.presenter  = [[AmountPresenter alloc] initWithInteractor:interactor];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}
- (void)testSaveAmount{
  // given
  int amount = 5000; //clp
  __block BOOL ok = false;
  
  // when
  [self.presenter saveAmount:amount success:^(id result) {
    ok = [@"OK" isEqualToString:result];
  } error:nil];
  
  //then
  XCTAssertTrue(ok);
}

@end
