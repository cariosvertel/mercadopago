//
//  AmountInteractor.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "BaseInteractor.h"

@interface AmountInteractor : BaseInteractor
- (void)saveAmount:(NSNumber *)number success:(InteractorOperationSuccess)success error:(InteractorOperationError)error;
@end
