//
//  PaymentMethodTableViewCell.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/15/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RLMBank.h"
#import "RLMPaymentMethod.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "BaseTableViewCell.h"


FOUNDATION_EXPORT NSString *const kPaymentMethodCellIdentifier;

@interface PaymentMethodTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *paymentImage;
@property (weak, nonatomic) IBOutlet UILabel *paymentName;

@end
