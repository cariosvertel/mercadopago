//
//  InteractorOperation.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/16/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSUInteger, OperationResult) {
  // operation was performed with ok result
  OperationRusultOK = 1,
  // operation was perforned with no errors but with nil result
  OperationResultNil,
};
@interface InteractorOperation : NSObject
@property(nonatomic, readonly, assign) OperationResult result;

-(instancetype)initWithResult:(OperationResult)result;
@end
