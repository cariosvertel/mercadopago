//
//  FeeViewController.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/15/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "FeeViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "FeeTableViewCell.h"

@interface FeeViewController ()<PaymentMethodTableViewCellDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic, strong) NSArray<Fee *> *fees;
@end

@implementation FeeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  
  
  __weak FeeViewController *weakSelf = self;
  self.title = kFeeControllerTitle;
  [self.presenter getPaymentDetail:^(Payment *payment) {
    [SVProgressHUD showWithStatus:kDownloadingFeeMessage];
    [weakSelf.presenter getFeesWithAmount:[payment.amount intValue] payment:payment.method bank:payment.bank success:^(NSArray<Fee *> *fees){
      
      weakSelf.fees = fees;
      [weakSelf.tableView reloadData];
      [SVProgressHUD dismiss];
      
      
    } error:^(NSError *error){
      [SVProgressHUD dismiss];
      [weakSelf showErrorWith:error];
    }];
  } error:^(NSError *error) {
    [weakSelf showErrorWith:error];
  }];
}
- (id)createPresenter{
  PaymentMethodsInteractor *interactor = [PaymentMethodsInteractor instance];
  return [[PaymentMethodsPresenter alloc] initWithInteractor:interactor];
}

#pragma mark - UITableViewCell Delegate & Datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
  return 117.0f;
  
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
  return 1.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  return self.fees.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  // for some reason this method doesn't work, I couldn't work with it.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  
  Fee *fee = self.fees[indexPath.row];
  FeeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kFeeTableViewCellIdentifier];
  [cell setObject:fee];
  cell.delegate = self;
  return cell;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Payment method
-(void)didTapOnCell:(Fee *)fee{
  __weak FeeViewController *weakSelf = self;
  [self.navigationController popToRootViewControllerAnimated:YES];
  [self.presenter getPaymentDetail:^(Payment *payment) {
    NSString *amountAsString = [NSString stringWithFormat:@"%d",[payment.amount intValue]];
    NSString *message = [NSString stringWithFormat:kFinalMessageBody,
                         [Utils formatAmount:amountAsString],
                         payment.method.name,
                         payment.bank.name,
                         fee.message];
    [weakSelf showMessageWithTitle:kFinalMessageTitle body:message];
  } error:^(NSError *error) {
    [weakSelf showErrorWith:error];
  }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
