//
//  RLMFee.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/15/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Realm/Realm.h>
#import "RealmObjectConverter.h"
#import "Fee.h"
@interface RLMFee : RLMObject<RealmObjectConverter>
@property NSString *identifier;
@property NSString *message;

+(NSArray<RLMFee *> *)decode:(NSArray<NSDictionary *> *)dictionary;
+(RLMFee *)decodeSingle:(NSDictionary *)payment;
@end
