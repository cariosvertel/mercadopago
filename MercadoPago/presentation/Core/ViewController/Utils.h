//
//  Utils.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/16/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject
+ (NSString *)formatAmount:(NSString *)amount;
+(NSString *)extractOnlyDigits:(NSString *)text;
@end
