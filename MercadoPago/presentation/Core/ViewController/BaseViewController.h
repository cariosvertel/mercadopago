//
//  BaseViewController.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController<__covariant PresenterType> : UIViewController
@property(nonatomic, strong) PresenterType presenter;
- (PresenterType)createPresenter;
- (void)showMessageWithTitle:(NSString *)title body:(NSString *)body;
- (void)showErrorWithMessage:(NSString *)errorMessage;
- (void)showErrorWith:(NSError *)error;
@end
