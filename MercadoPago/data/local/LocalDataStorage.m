//
//  LocalDataStorage.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "LocalDataStorage.h"


@interface LocalDataStorage()
@end
@implementation LocalDataStorage
- (instancetype)init{
  self = [super init];
  if (self){
  }
  return self;
}
- (RLMRealm *)realmInstance{
  return [RLMRealm defaultRealm];
}


#pragma mark -
- (id)fetchFirstObjectOfClass:(Class)clazz{
  
  if ([[clazz description] isEqualToString:[Payment.class description]]) {
    return [[[RLMPayment allObjects] firstObject] fromRealmObject];
  }
  return nil;
}
- (void) save:(id)object{
  // TODO: implementation
  
  RLMObject *rObject = nil;
  
  if ([object isKindOfClass:Payment.class]){
    rObject = [RLMPayment toRealmObject:object];
    
  }
  RLMRealm *realm = [self realmInstance];
  
  [realm transactionWithBlock:^{
    [realm addOrUpdateObject:rObject];
  }];
}

@end
