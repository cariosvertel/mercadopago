//
//  PaymentMethodTableViewCell.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/15/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "PaymentMethodTableViewCell.h"


NSString *const kPaymentMethodCellIdentifier = @"paymentTableViewCell";

@implementation PaymentMethodTableViewCell
-(void) setObject:(id)object{
  [super setObject:object];
  if ([object isKindOfClass:PaymentMethod.class]){
    RLMPaymentMethod *method = (RLMPaymentMethod *)object;
    self.paymentName.text = method.name;
    // this method should be invoked from backend, but currently I'm not have time.
    [self.paymentImage setImageWithURL:[NSURL URLWithString:method.imageUrl]];
    
  }
  if ([object isKindOfClass:Bank.class]){
    RLMBank *bank = (RLMBank *)object;
    
    self.paymentName.text = bank.name;
    // this method should be invoked from backend, but currently I'm not have time.
    [self.paymentImage setImageWithURL:[NSURL URLWithString:bank.imageUrl]];
  }
}
-(void)prepareForReuse{
  [super prepareForReuse];
  self.paymentImage.image = nil;
  self.paymentName.text = nil;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];
  
  // Configure the view for the selected state
}

@end
