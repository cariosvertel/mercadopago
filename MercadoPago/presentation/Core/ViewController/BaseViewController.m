//
//  BaseViewController.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  self.presenter = [self createPresenter];
  [self setupUI];
}
- (id)createPresenter{
  // to be overriden
  return nil;
}
-(void)setupUI{
  UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnMainView:)];
  [self.view addGestureRecognizer:recognizer];
  
}
-(void)didTapOnMainView:(UIGestureRecognizer *)aRecognizer{
  [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)showMessageWithTitle:(NSString *)title body:(NSString *)body{
  UIAlertController * alert=   [UIAlertController
                                alertControllerWithTitle:title
                                message:body
                                preferredStyle:UIAlertControllerStyleAlert];
  
  UIAlertAction* okButton = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                               [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
  
  [alert addAction:okButton];
  [self presentViewController:alert animated:YES completion:nil];
  
}
- (void)showErrorWith:(NSError *)error{
  NSString *errorString = [error userInfo][NSLocalizedDescriptionKey];
  [self showErrorWithMessage:errorString];
  
}
- (void)showErrorWithMessage:(NSString *)errorMessage{
  [self showMessageWithTitle:@"Error" body:errorMessage];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
