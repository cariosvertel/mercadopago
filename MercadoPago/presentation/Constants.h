//
//  Constants.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/16/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define BLUE_MERCADO_PAGO [UIColor colorWithRed:26.0/255.0 green:159.0/255.0 blue:224.0/255.0 alpha:1]

// titles
static NSString *const kAmountControllerTitle = @"Ingrese monto";
static NSString *const kPaymentMethodControllerTitle = @"Metodo de pago";
static NSString *const kDownloadingPaymentMethodsMessage = @"Descargando metodos de pagos...";
static NSString *const kBankControllerTitle = @"Seleccione Banco";
static NSString *const kDownloadingBanksMessage = @"Descargando bancos...";
static NSString *const kFeeControllerTitle = @"Cuotas";
static NSString *const kDownloadingFeeMessage = @"Descargando cuotas de pago...";

// final message
static NSString *const kFinalMessageBody = @"Monto del pago: %@\n Metodo de pago: %@\n Banco: %@\n Cuotas: %@";
static NSString *const kFinalMessageTitle = @"Información";


#endif /* Constants_h */
