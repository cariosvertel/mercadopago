//
//  BankViewController.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/15/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "BaseViewController.h"
#import "PaymentMethodsPresenter.h"
#import "PaymentMethodTableViewCell.h"
#import "Constants.h"

@interface BankViewController : BaseViewController<PaymentMethodsPresenter *>

@end
