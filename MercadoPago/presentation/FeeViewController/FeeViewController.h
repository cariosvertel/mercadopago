//
//  FeeViewController.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/15/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "BaseViewController.h"
#import "PaymentMethodsPresenter.h"
#import "Constants.h"
#import "Utils.h"


@interface FeeViewController : BaseViewController<PaymentMethodsPresenter *>
@property(nonatomic, strong) RLMPaymentMethod *selectedPayment;
@property(nonatomic, strong) RLMBank *selectedBank;
@end
