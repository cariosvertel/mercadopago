//
//  RLMFee.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/15/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "RLMFee.h"

@implementation RLMFee
+(NSArray<RLMFee *> *)decode:(NSArray<NSDictionary *> *)dictionary{
  
  NSMutableArray *newArray = [NSMutableArray new];
  for (NSDictionary *paymentAsJson in dictionary){
    [newArray addObject:[RLMFee decodeSingle:paymentAsJson]];
  }
  return newArray;
}
+(RLMFee *)decodeSingle:(NSDictionary *)payment{
  RLMFee *newFee = [[RLMFee alloc] init];
  newFee.identifier = payment[@"installments"];
  newFee.message = payment[@"recommended_message"];
  return newFee;
}
+(NSString *)primaryKey {
  return @"identifier";
}
#pragma mark - converter objects
-(Fee *)fromRealmObject{
  Fee *f = [[Fee alloc] initWithIdentifier:self.identifier];
  f.message = self.message;
  return f;
}
+(RLMFee *)toRealmObject:(Fee *)obj{
  RLMFee *rf = [[RLMFee alloc] init];
  rf.identifier = obj.identifier;
  rf.message = obj.identifier;
  return rf;
}

@end
