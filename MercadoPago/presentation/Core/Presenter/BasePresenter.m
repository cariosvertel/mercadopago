//
//  BasePresenter.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "BasePresenter.h"
@interface BasePresenter()
//@property(nonatomic, strong) PresenterSuccess callback;
//@property(nonatomic, strong) PresenterError errorCallback;
@end

@implementation BasePresenter
@synthesize interactor = _interactor;

- (instancetype) initWithInteractor:(id)interactor{
  self = [super init];
  if (self){
    _interactor = interactor;
  }
  return self;
}


@end
