//
//  Payment.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaymentMethod.h"
#import "Bank.h"
#import "BaseObject.h"

@interface Payment : BaseObject
@property(nonatomic, strong) NSNumber *amount;
@property(nonatomic, strong) PaymentMethod *method;
@property(nonatomic, strong) Bank *bank;
@end
