//
//  BankViewController.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/15/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "BankViewController.h"
#import "Bank.h"
#import "Payment.h"
#import <SVProgressHUD/SVProgressHUD.h>


NSString *const kGoToFees = @"goToFees";
@interface BankViewController ()<PaymentMethodTableViewCellDelegate>
@property(nonatomic, strong) NSArray<Bank *> *banks;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end

@implementation BankViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  self.banks = [NSArray new];
  __weak BankViewController *weakSelf = self;
  
  self.title = kBankControllerTitle;
  [SVProgressHUD showWithStatus:kDownloadingBanksMessage];
  [self.presenter getPaymentDetail:^(Payment *paymentRequest) {
    
    [weakSelf.presenter getBanksForPayment:paymentRequest.method success:^(NSArray<Bank *>  *banks) {
      weakSelf.banks = banks;
      [weakSelf.tableView reloadData];
      [SVProgressHUD dismiss];
      
    } error:^(NSError *error) {
      [SVProgressHUD dismiss];
      [weakSelf showErrorWith:error];
    }];
  } error:nil];
}
- (id)createPresenter{
  PaymentMethodsInteractor *interactor = [PaymentMethodsInteractor instance];
  return [[PaymentMethodsPresenter alloc] initWithInteractor:interactor];
}
#pragma mark - UITableViewCell Delegate & Datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
  return 48.0f;
  
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
  return 1.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  return self.banks.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  // for some reason this method doesn't work, I couldn't work with it.
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  
  Bank *method = self.banks[indexPath.row];
  PaymentMethodTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kPaymentMethodCellIdentifier];
  [cell setObject:method];
  cell.delegate = self;
  return cell;
  
}
#pragma mark - Payment method
-(void)didTapOnCell:(Bank *)bank{
  __weak BankViewController *weakSelf = self;
  [self.presenter saveBank:bank success:^(id result){
    [weakSelf performSegueWithIdentifier:kGoToFees sender:nil];
  } error:^(NSError *error){
    [weakSelf showErrorWith:error];
  }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
