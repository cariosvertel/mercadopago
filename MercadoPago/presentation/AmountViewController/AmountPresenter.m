//
//  AmountPresenter.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "AmountPresenter.h"
NSString *const kAmountPresenterDomain = @"AmountPresenter";
@implementation AmountPresenter
- (void) saveAmount:(NSString *)amount success:(PresenterOperationSuccess)success error:(PresenterOperationError)error{
  NSString *onlyDigits = [Utils extractOnlyDigits:amount];
  if(onlyDigits == nil || onlyDigits.length == 0){
    error([NSError errorWithDescription:@"Monto invalido" domain:kAmountPresenterDomain]);
    return;
  }
  NSNumber *amountAsNumber = [NSNumber numberWithInt:[onlyDigits intValue]];
  [self.interactor saveAmount:amountAsNumber success:success error:error];
}
- (NSString *)formatAmount:(NSString *)text{
  return  [Utils formatAmount:text];
}
@end
