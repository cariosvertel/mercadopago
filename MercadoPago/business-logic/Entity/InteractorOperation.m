//
//  InteractorOperation.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/16/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "InteractorOperation.h"

@implementation InteractorOperation
@synthesize result = _result;
-(instancetype)initWithResult:(OperationResult)result{
  self = [super init];
  if (self){
    _result = result;
  }
  return self;
}
@end
