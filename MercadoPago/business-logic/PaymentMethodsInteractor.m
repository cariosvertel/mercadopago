//
//  PaymentMethodsInteractor.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "PaymentMethodsInteractor.h"
NSString *const kPaymentMethodInteractorDomain = @"PaymentMethodInteractorDomain";
@implementation PaymentMethodsInteractor
-(void)getPaymentMethods:(InteractorOperationSuccess)success error:(InteractorOperationError)error{
  [self.storage.cloud getPaymentsMethods:^(NSArray<PaymentMethod *> *result) {
    
    NSPredicate *excludeNoCreditCard = [NSPredicate predicateWithFormat:@"methodType CONTAINS 'credit_card'"];
    NSArray<PaymentMethod *> *filtered = [result filteredArrayUsingPredicate:excludeNoCreditCard];
    success(filtered);
  } failed:error];
}
-(void) getBanksForPayment:(PaymentMethod *)payment success:(InteractorOperationSuccess)success error:(InteractorOperationError)error{
  [self.storage.cloud getBankForPaymentId:payment.identifier success:^(NSArray<Bank *> *banks){
    if (!(banks.count > 0)){
      error([NSError errorWithDescription:@"No existen bancos para este metodo de pago"
                                   domain:kPaymentMethodInteractorDomain]);
      return;
    }
    success(banks);
  } failed:error];
  
}
- (void) getFeesWithAmount:(int)amount payment:(PaymentMethod *)payment bank:(Bank *)bank success:(InteractorOperationSuccess)success error:(InteractorOperationError)error{
  NSString *amountAsString = [NSString stringWithFormat:@"%d", amount];
  [self.storage.cloud getFeesWithAmount:amountAsString paymentId:payment.identifier bank:bank.identifier success:success failed:error];
}
- (void)savePaymentMethod:(PaymentMethod *)pm success:(InteractorOperationSuccess)success error:(InteractorOperationError)error{
  [self fetchFirstObjectOfClass:Payment.class sucess:^(Payment *payment) {
    dispatch_async(self.scheduler, ^{
    
      if (payment != nil) {
        payment.method = pm;
        [self.storage.local save:payment];
        
        [self runOnMainThread:^{
          success([[InteractorOperation alloc] initWithResult:OperationRusultOK]);
        }];
        return;
      }
      [self runOnMainThread:^{
        NSError *newError = [NSError errorWithDescription:@"No existe objecto payment en la base de datos"
                                                   domain:kPaymentMethodInteractorDomain];
        error(newError);
      }];
    });
  } error:error];
  
}
- (void)saveBank:(Bank *)bank success:(InteractorOperationSuccess)success error:(InteractorOperationError)error{
  [self fetchFirstObjectOfClass:Payment.class sucess:^(Payment *payment) {
    dispatch_async(self.scheduler, ^{
      
      if (payment != nil) {
        payment.bank = bank;
        [self.storage.local save:payment];
        
        [self runOnMainThread:^{
          success([[InteractorOperation alloc] initWithResult:OperationRusultOK]);
        }];
        return;
      }
      [self runOnMainThread:^{
        NSError *newError = [NSError errorWithDescription:@"No existe objecto payment en la base de datos"
                                                   domain:kPaymentMethodInteractorDomain];
        error(newError);
      }];
    });
  } error:error];
}

@end
