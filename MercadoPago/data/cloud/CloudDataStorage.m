//
//  CloudDataStorage.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "CloudDataStorage.h"
NSString *const kCloudStorageDomain = @"CloudStorage";

@implementation CloudDataStorage
@synthesize adapter = _adapter;
- (instancetype)initWithAdapter:(id<NetworkAdapter>)adapter{
  self = [super init];
  if (self){
    _adapter = adapter;
  }
  return self;
}
- (void) getPaymentsMethods:(CloudOperationSuccess)success failed:(CloudOperationError)failed{
  NSString *endpoint = @"https://api.mercadopago.com/v1/payment_methods?public_key=444a9ef5-8a6b-429f-abdf-587639155d88";
  [self.adapter get:endpoint parameters:nil callback:^(id result, NSError *error) {
    if(result){
      NSArray<PaymentMethod *> *paymentsMethods = [[RLMPaymentMethod decode:result] map:^id(RLMPaymentMethod *pm) {
        return [pm fromRealmObject];
      }];
      success(paymentsMethods);
    }else if(error != nil){
      failed(error);
    }
  }];
}
- (void) getBankForPaymentId:(NSString *)paymentId success:(CloudOperationSuccess)success failed:(CloudOperationError)failed{
  //
  NSString *endpoint = [NSString stringWithFormat:@"https://api.mercadopago.com/v1/payment_methods/card_issuers?public_key=444a9ef5-8a6b-429f-abdf-587639155d88&payment_method_id=%@",paymentId];
#if DEBUG
  NSLog(@"FETCHING BANK: %@", endpoint);
#endif
  [self.adapter get:endpoint parameters:nil callback:^(id result, NSError *error) {
    if (result){
      NSArray<Bank *> *banks = [[RLMBank decode:result] map:^id(RLMBank *b) {
        return [b fromRealmObject];
      }];
      success(banks);
    }else if(error != nil){
      failed(error);
    }
  }];
}
- (void) getFeesWithAmount:(NSString *)amount paymentId:(NSString *)paymentId bank:(NSString *)bankId success:(CloudOperationSuccess)success failed:(CloudOperationError)failed{
  
  NSString *endPoint = [NSString stringWithFormat:@"https://api.mercadopago.com/v1/payment_methods/installments?public_key=444a9ef5-8a6b-429f-abdf-587639155d88&amount=%@&payment_method_id=%@&issuer.id=%@",amount, paymentId, bankId];
#if DEBUG
  NSLog(@"FETCHING WIH: %@ PAYMENTID: %@ BANKID: %@", amount, paymentId, bankId);
#endif
  
  [self.adapter get:endPoint parameters:nil callback:^(id result, NSError *error) {
    
    NSArray<NSDictionary *> *feesAsJson = (NSArray<NSDictionary *> *)result;
    if (feesAsJson != nil && feesAsJson.count > 0) {
      NSDictionary *feesObject = feesAsJson[0];
      NSArray<NSDictionary *> *fees = [feesObject objectForKey:@"payer_costs"];
      if (fees != nil && fees.count > 0){
        NSArray<Fee *> *feesAsObject = [[RLMFee decode:fees] map:^id(RLMFee *rFee) {
          return [rFee fromRealmObject];
        }];
        success(feesAsObject);
      }else{
        failed([NSError errorWithDescription:@"Respuesta no contiene cuotas" domain:kCloudStorageDomain]);
      }
    }else{
      failed([NSError errorWithDescription:@"Respuesta no tiene banco asociado" domain:kCloudStorageDomain]);
    }
  }];
  
  
  
}

@end
