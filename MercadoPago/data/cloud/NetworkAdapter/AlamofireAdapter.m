//
//  AlamofireAdapter.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "AlamofireAdapter.h"

@implementation AlamofireAdapter

- (void)url:(NSString *)url parameters:(NSDictionary *)parameters callback:(NetworkCallback)callback {

}
- (void)get:(NSString *)url parameters:(id)parameters callback:(NetworkCallback)callback{
  
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  
  [manager GET:url parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    //succ
    NSDictionary *dictionary = (NSDictionary *)responseObject;
    if (dictionary != nil){
      callback(dictionary, nil);
    }else{
      NSDictionary *info = @{NSLocalizedDescriptionKey:@"Unknown error from network"};
      NSError *error = [NSError errorWithDomain:@"Network" code:0 userInfo:info];
      callback(nil, error);
    }
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    callback(nil, error);
  }];
  
  
  
  
  
}
@end
