//
//  Bank.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/15/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseObject.h"

@interface Bank : BaseObject
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *imageUrl;

@end
