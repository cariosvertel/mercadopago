//
//  AmountViewController.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "BaseViewController.h"
#import "AmountPresenter.h"
#import "Constants.h"

@interface AmountViewController : BaseViewController<AmountPresenter *>

@end
