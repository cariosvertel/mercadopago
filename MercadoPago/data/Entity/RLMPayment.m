//
//  RLMPayment.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "RLMPayment.h"

@interface RLMPayment()<RealmObjectConverter>
@end
@implementation RLMPayment


#pragma mark - conversion
- (id)fromRealmObject {
  Payment *newPayment = [[Payment alloc] initWithIdentifier:self.identifier];
  newPayment.amount = [NSNumber numberWithInt:self.amount];
  if (self.paymentMethod != nil){
    newPayment.method = [self.paymentMethod fromRealmObject];
  }
  if (self.bank != nil){
    newPayment.bank = [self.bank fromRealmObject];
  }
  return newPayment;
}

+ (id)toRealmObject:(Payment *)obj {
  
  RLMPayment *rPayment = [[RLMPayment alloc] init];
  rPayment.identifier = obj.identifier;
  rPayment.amount = [obj.amount intValue];
  if (obj.method != nil){
    rPayment.paymentMethod = [RLMPaymentMethod toRealmObject:obj.method];
  }
  if (obj.bank != nil){
    rPayment.bank = [RLMBank toRealmObject:obj.bank];
  }
  return rPayment;
}
+ (NSString *)primaryKey {
  return @"identifier";
}

@end
