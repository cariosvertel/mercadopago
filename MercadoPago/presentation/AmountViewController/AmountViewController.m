//
//  AmountViewController.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "AmountViewController.h"

NSString *const kGoToPayments = @"goToPayments";

@interface AmountViewController ()
@property (weak, nonatomic) IBOutlet UITextField *amountLabel;
@property (weak, nonatomic) IBOutlet UITextField *amountInput;

@end

@implementation AmountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  self.title = kAmountControllerTitle;
  
  
}
- (id)createPresenter{
  AmountInteractor *interactor = [AmountInteractor instance];
  return [[AmountPresenter alloc] initWithInteractor:interactor];
}

#pragma mark - UIActions

- (IBAction)didPressContinue:(id)sender {
  
  NSString *value = self.self.amountLabel.text ;
  __weak AmountViewController *weakSelf = self;
  [self.presenter saveAmount:value success:^(id result){
    [weakSelf performSegueWithIdentifier:kGoToPayments sender:nil];
  }error:^(NSError *error) {
    [weakSelf showErrorWith:error];
  }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextField Delegate
- (IBAction)didChangeTextFieldValue:(UITextField *)sender
{
  self.amountInput.text = [self.presenter formatAmount:sender.text];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
