//
//  CloudDataStorage.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CloudStorage.h"
#import "NetworkAdapter.h"
#import "RLMPaymentMethod.h"
#import "RLMBank.h"
#import "NSError+Utilities.h"
#import "RLMFee.h"
#import "NSArray+Utilities.h"

@interface CloudDataStorage : NSObject<CloudStorage>
@property(nonatomic, strong,readonly) id<NetworkAdapter> adapter;

- (instancetype)initWithAdapter:(id<NetworkAdapter>)adapter;
- (void) getPaymentsMethods:(CloudOperationSuccess)success failed:(CloudOperationError)failed;
- (void) getFeesWithAmount:(NSString *)amount paymentId:(NSString *)paymentId bank:(NSString *)bankId success:(CloudOperationSuccess)success failed:(CloudOperationError)failed;
@end
