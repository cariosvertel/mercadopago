//
//  FakeAmountView.h
//  MercadoPagoTests
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AmountViewController.h"

@interface FakeAmountView : NSObject<AmountView>

@end
