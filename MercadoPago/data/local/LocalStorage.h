//
//  LocalStorage.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#ifndef LocalStorage_h
#define LocalStorage_h

@protocol LocalStorage<NSObject>
- (void)save:(id)object;
- (id)fetchFirstObjectOfClass:(Class)clazz;
@end

#endif /* LocalStorage_h */
