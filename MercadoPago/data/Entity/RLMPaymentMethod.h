//
//  PaymentMethod.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Realm/Realm.h>
#import "RealmObjectConverter.h"
#import "PaymentMethod.h"

@interface RLMPaymentMethod : RLMObject<RealmObjectConverter>
@property NSString *identifier;
@property NSString *name;
@property NSString *methodType;
@property NSString *imageUrl;

+(NSArray<RLMPaymentMethod *> *)decode:(NSArray<NSDictionary *> *)dictionary;
+(RLMPaymentMethod *)decodeSingle:(NSDictionary *)payment;
@end
