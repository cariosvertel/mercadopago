//
//  RealmObjectConverter.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#ifndef RealmObjectConverter_h
#define RealmObjectConverter_h
@protocol RealmObjectConverter<NSObject>
+(id)toRealmObject:(id)obj;
-(id)fromRealmObject;
@end

#endif /* RealmObjectConverter_h */
