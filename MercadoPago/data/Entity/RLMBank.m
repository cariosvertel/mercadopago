//
//  RLMBank.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/15/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "RLMBank.h"

@implementation RLMBank

+(NSArray<RLMBank *> *)decode:(NSArray<NSDictionary *> *)dictionary{
  
  NSMutableArray *newArray = [NSMutableArray new];
  for (NSDictionary *paymentAsJson in dictionary){
    [newArray addObject:[RLMBank decodeSingle:paymentAsJson]];
  }
  return newArray;
}
+(RLMBank *)decodeSingle:(NSDictionary *)payment{
  RLMBank *newBank = [[RLMBank alloc] init];
  newBank.name = payment[@"name"];
  newBank.identifier = payment[@"id"];
  newBank.imageUrl = payment[@"thumbnail"];
  return newBank;
}
+(NSString *)primaryKey {
  return @"identifier";
}
#pragma mark - Realm Object Converter
-(Bank *)fromRealmObject{
  Bank *b = [[Bank alloc] initWithIdentifier:self.identifier];
  b.name = self.name;
  b.imageUrl = self.imageUrl;
  return  b;
}
+(RLMBank *)toRealmObject:(Bank *)obj{
  RLMBank *rb = [[RLMBank alloc] init];
  rb.identifier = obj.identifier;
  rb.imageUrl = obj.imageUrl;
  rb.name = obj.name;
  return rb;
}
@end
