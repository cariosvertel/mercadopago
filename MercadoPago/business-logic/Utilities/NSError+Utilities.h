//
//  NSError+Utilities.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/15/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (Utilities)
+(NSError *) errorWithDescription:(NSString *)description domain:(NSString *)domain;
@end
