//
//  AlamofireAdapter.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/14/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkAdapter.h"
#import <AFNetworking/AFNetworking.h>

@interface AlamofireAdapter : NSObject<NetworkAdapter>

@end
