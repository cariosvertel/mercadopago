//
//  BaseObject.m
//  MercadoPago
//
//  Created by Carlos Rios on 6/15/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import "BaseObject.h"

@implementation BaseObject
@synthesize identifier = _identifier;
-(instancetype)init{
  self = [super init];
  if (self){
    _identifier = [[NSProcessInfo processInfo] globallyUniqueString];
  }
  return self;
}
-(instancetype)initWithIdentifier:(NSString *)identifier{
  self = [super init];
  if (self){
    _identifier = identifier;
  }
  return self;
}
@end
