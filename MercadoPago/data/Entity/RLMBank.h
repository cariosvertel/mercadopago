//
//  RLMBank.h
//  MercadoPago
//
//  Created by Carlos Rios on 6/15/18.
//  Copyright © 2018 Mercado Libre. All rights reserved.
//

#import <Realm/Realm.h>
#import "RealmObjectConverter.h"
#import "Bank.h"

@interface RLMBank : RLMObject<RealmObjectConverter>
@property NSString *identifier;
@property NSString *name;
@property NSString *imageUrl;

+(NSArray<RLMBank *> *)decode:(NSArray<NSDictionary *> *)dictionary;
+(RLMBank *)decodeSingle:(NSDictionary *)payment;
@end
